package it.unibo.oop.lab.nesting2;

import java.util.Iterator;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final List<T> list;
	
	public OneListAcceptable(List<T> list) {
		this.list = list;
	}
	
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			Iterator<T> iterator = OneListAcceptable.this.list.iterator();
			
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(!(iterator.hasNext() && iterator.next() == newElement)) {
					throw new Acceptor.ElementNotAcceptedException(newElement);
				}
			}

			public void end() throws EndNotAcceptedException {
				if(iterator.hasNext()) {
					throw new Acceptor.EndNotAcceptedException();
				}
			}
		};
	}
}
