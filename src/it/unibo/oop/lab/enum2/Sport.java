/**
 * 
 */
package it.unibo.oop.lab.enum2;
import static it.unibo.oop.lab.enum2.Place.*;
/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	BASKET(INDOOR,5,"Basket"),
	SOCCER(OUTDOOR,11,"Soccer"),
	TENNIS(OUTDOOR,1,"Tennis"),
	BIKE(OUTDOOR,1,"Bike"),
	F1(OUTDOOR,1,"Formula 1"),
	MOTOGP(OUTDOOR,1,"MotoGP"),
	VOLLEY(INDOOR,6,"Volley");

	private final int nMembers;
	private final String name;
	private final Place place;

	private Sport(final Place place, final int noTeamMembers, final String actualName) {
		this.place = place;
		this.nMembers = noTeamMembers;
		this.name = actualName;
	}
	
	/**
	 * @return true if the number of members is 1
	 */
	public boolean isIndividualSport() {
		return this.nMembers == 1;
	}
	
	/**
	 * @return true if is an Indoor sport
	 */
	public boolean isIndoorSport() {
		return this.place == INDOOR;
	}
	
	/**
	 * @return the sport's place
	 */
	public Place getPlace() {
		return this.place;
	}
	
	/**
	 * @return Sport in string format
	 */
	public String toString() {
		return this.name + ", " + this.place + ", " + this.nMembers + " players"; 
	}
	
	
}
